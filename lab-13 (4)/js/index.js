function flight (airline, number, origin, destination, dep_time, arrival_time, arrival_gate) {
  this.airline = airline;
  this.number = number;
  this.origin = origin;
  this.destination = destination;
  this.dep_time = dep_time;
  this.arrival_time = arrival_time;
  this.arrival_gate = arrival_gate;
  this.flight_duration = function() {
    return this.arrival_time - this.dep_time;
  }
}
var alaska = new flight(
  "Alaska",
  "ASA10",
  "KSEA",
  "KJFK",
  new Date("NOV 11, 2018 19:00:00"),
  new Date("NOV 12, 2018 01:00:00"),
  "B10"
);
var delta = new flight(
  "Delta",
  "ASA1168",
  "KLAX",
  "KEWR",
  new Date("NOV 11, 2018 22:59:00"),
  new Date("NOV 12, 2018 05:39:00"),
  "D9");
var united = new flight(
  "United",
  "ASA124",
  "FAI",
  "KSEA",
  new Date("NOV 11, 2018 18:08:00"),
  new Date("NOV 11, 2018 22:25:00"),
  "A11");
var britishAirways = new flight(
  "British Airways",
  "ASA1419",
  "KLAS",
  "KPDX",
  new Date("NOV 11, 2018 21:04:00"),
  new Date("NOV 11 2018 23:00:00"),
  "B7");
var americanAirlines = new flight(
  "American Airlines",
  "ASA1421",
  "KSJC",
  "KSEA",
  new Date("NOV 11 2018 23:47:00"),
  new Date("NOV 12 2018 01:59:00"),
  "C4");
var flights = [alaska, delta, united, britishAirways, americanAirlines];
 

function milisToMinutesAndSeconds(duration) {
    var milliseconds = parseInt((duration%1000)/100)
        , seconds = parseInt((duration/1000)%60)
        , minutes = parseInt((duration/(1000*60))%60)
        , hours = parseInt((duration/(1000*60*60))%24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;
}






var el = document.getElementById('tableData');
el.innerHTML = "<th>Airline</th> <th>Number</th> <th>Origin</th> <th> Destination</th> <th>Departure Time</th> <th>Arrival Time</th> <th> Arrival Gate</th> <th>Flight Duration</th>"

var i = 0;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] +"</td><td>" + flights[i]['origin'] +"</td><td>"
+ flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] +"</td><td>"+flights[i]['arrival_gate']+ "</td><td>"+ milisToMinutesAndSeconds(alaska.flight_duration()) + "</td></tr>";

i = 1;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] +"</td><td>" + flights[i]['origin'] +"</td><td>"
+flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" +flights[i]['arrival_time'] +"</td><td>"+flights[i]['arrival_gate']+ "</td><td>"+ milisToMinutesAndSeconds(delta.flight_duration()) + "</td></tr>";

i = 2;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] +"</td><td>" + flights[i]['origin'] +"</td><td>"
+flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" +flights[i]['arrival_time'] +"</td><td>"+flights[i]['arrival_gate']+ "</td><td>"+ milisToMinutesAndSeconds(united.flight_duration()) + "</td></tr>";

i = 3;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] +"</td><td>" + flights[i]['origin'] +"</td><td>"
+flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" +flights[i]['arrival_time'] +"</td><td>"+flights[i]['arrival_gate'] + "</td><td>"+ milisToMinutesAndSeconds( britishAirways.flight_duration()) + "</td></tr>";

i = 4;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] +"</td><td>" + flights[i]['origin'] +"</td><td>"
+flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>"+ flights[i]['arrival_time'] +"</td><td>"+flights[i]['arrival_gate'] + "</td><td>"+ milisToMinutesAndSeconds(americanAirlines.flight_duration()) + "</td></tr>";